﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Statki
{
    /// <summary>
    /// Interaction logic for Bitwa.xaml
    /// </summary>
    public partial class Bitwa : UserControl
    {
         
            public event EventHandler replay;
            public Grid[] playerGrid;
            public Grid[] compGrid;
            public List<int> hitList;
            int turnCount = 0;
            public Random random = new Random();
            public string playerName;
            

            int pPieciomasztowiecCount = 5, cPieciomasztowiecCount = 5;
            int pCzteromasztowiecCount = 4, cCzteromasztowiecCount = 4;
            int pTrojmasztowiec1Count = 3, cTrojmasztowiec1Count = 3;
            int pTrojmasztowiec2Count = 3, cTrojmasztowiec2Count = 3;
            int pDwumasztowiecCount = 2, cDwumasztowiecCount = 2;

            public Bitwa(Grid[] playerGrid, string playerName)
            {

                InitializeComponent();
                initiateSetup(playerGrid);
            this.playerName = playerName;
            hitList = new List<int>();
            }

            private void initiateSetup(Grid[] userGrid)
            {
       
                compGrid = new Grid[110];
                CompGrid.Children.CopyTo(compGrid, 0);
                for (int i = 0; i < 110; i++)
                {
                    compGrid[i].Tag = "woda";
                }
                setupCompGrid();
               
                playerGrid = new Grid[110];
                PlayerGrid.Children.CopyTo(playerGrid, 0);

          
                for (int i = 0; i < 110; i++)
                {
                    playerGrid[i].Background = userGrid[i].Background;
                    playerGrid[i].Tag = userGrid[i].Tag;
                }
            }


            private void setupCompGrid()
            {
                Random random = new Random();
                int[] shipSizes = new int[] { 2, 3, 3, 4, 5 };
                string[] ships = new string[] { "dwumasztowiec", "trojmasztowiec1", "trojmasztowiec2", "czteromasztowiec", "pieciomasztowiec" };
                int size, index;
                string ship;
                Orientation orientation;
                bool unavailableIndex = true;

                for (int i = 0; i < shipSizes.Length; i++)
                {
                    
                    size = shipSizes[i];
                    ship = ships[i];
                    unavailableIndex = true;

                    if (random.Next(0, 2) == 0)
                        orientation = Orientation.Horizontal;
                    else
                        orientation = Orientation.Vertical;

                    
                    if (orientation.Equals(Orientation.Horizontal))
                    {
                        index = random.Next(0, 110);
                        while (unavailableIndex == true)
                        {
                            unavailableIndex = false;

                            while ((index + size - 1) % 10 < size - 1)
                            {
                                index = random.Next(0, 110);
                            }

                            for (int j = 0; j < size; j++)
                            {
                                if (index + j > 109 )
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }

                                if (!compGrid[index + j].Tag.Equals("woda"))
                            {
                                index = random.Next(0, 110);
                                unavailableIndex = true;
                                break;
                            }

                            if (!compGrid[index + j].Tag.Equals("woda"))
                            {
                                index = random.Next(0, 110);
                                unavailableIndex = true;
                                break;
                            }

                            if (index > 9)
                            {
                                if (!compGrid[index - 10].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                                if (!compGrid[index - 9].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                                if (!compGrid[index + j - 9].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }

                                if (index % 10 != 0)
                                {
                                    if (!compGrid[index - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index - 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }

                            }

                            if (index != 107)
                            {
                                if (!compGrid[index + j + 1].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                            }

                            if (index < 93)
                            {
                                if (!compGrid[index + j + 10].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                                if (!compGrid[index + 9].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }

                                if (!compGrid[index + j + 11].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                            }


                            if (index < 9 && index != 0)
                            {
                                if (!compGrid[index - 1].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                            }
                        }
                        }
                        for (int j = 0; j < size; j++)
                        {
                            compGrid[index + j].Tag = ship;
                       }
                    }
                    else
                    {
                        index = random.Next(0, 110);
                        while (unavailableIndex == true)
                        {
                            unavailableIndex = false;

                            while (index / 10 + size * 10 > 110)
                            {
                                index = random.Next(0, 110);
                            }

                            for (int j = 0; j < size * 10; j += 10)
                            {
                                if (index + j > 109)
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                                if (!compGrid[index + j].Tag.Equals("woda"))
                            {
                                index = random.Next(0, 110);
                                unavailableIndex = true;
                                break;
                            }

                            if (index > 9)
                            {
                                if (index % 10 != 0)
                                {
                                    if (!compGrid[index - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index + j - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index - 10].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }

                                    if (!compGrid[index - 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if ((index + 1) % 10 != 0)
                                    {
                                        if (!compGrid[index + 1].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!compGrid[index + j + 1].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!compGrid[index - 9].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if ((index + j) < 99)
                                        {
                                            if (!compGrid[index + j + 11].Tag.Equals("woda"))
                                            {
                                                index = random.Next(0, 110);
                                                unavailableIndex = true;
                                                break;
                                            }
                                            if (!compGrid[index + 11].Tag.Equals("woda"))
                                            {
                                                index = random.Next(0, 110);
                                                unavailableIndex = true;
                                                break;
                                            }
                                        }
                                    }
                                    if ((index + j) < 99)
                                    {
                                        if (!compGrid[index + j + 10].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!compGrid[index + j + 9].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!compGrid[index + 9].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }

                                    }
                                }


                                else
                                {
                                    if (!compGrid[index + 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index + j + 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index - 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index - 10].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index + 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }

                                    if ((index + j) < 99)
                                    {
                                        if (!compGrid[index + j + 10].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!compGrid[index + j + 11].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                    }
                                }

                            }

                            else
                            {
                                if (!compGrid[index + j + 10].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                                if (index != 0)
                                {
                                    if (!compGrid[index - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index + 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }
                                if (index != 9)
                                {
                                    if (!compGrid[index + 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!compGrid[index + 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }
                            }

                        }
                        }
                        for (int j = 0; j < size * 10; j += 10)
                        {
                            compGrid[index + j].Tag = ship;
                          
                        }
                    }

                }


            }

            private void gridMouseDown(object sender, MouseButtonEventArgs e)
            {
                Grid square = (Grid)sender;

                if (turnCount % 2 != 0)
                {
                    return;
                }

                switch (square.Tag.ToString())
                {
                    case "woda":
                        square.Tag = "pudło";
                        square.Background = new SolidColorBrush(Colors.LightGray);
                        turnCount++;
                        compTurn();
                        return;
                    case "pudło":
                    case "trafiony":
                        Console.WriteLine("Trafiony");
                        return;
                    case "dwumasztowiec":
                        cDwumasztowiecCount--;
                        break;
                    case "trojmasztowiec2":
                        cTrojmasztowiec2Count--;
                        break;
                    case "trojmasztowiec1":
                        cTrojmasztowiec1Count--;
                        break;
                    case "czteromasztowiec":
                        cCzteromasztowiecCount--;
                        break;
                    case "pieciomasztowiec":
                        cPieciomasztowiecCount--;
                        break;
                }
                square.Tag = "trafiony";
                square.Background = new SolidColorBrush(Colors.Red);
                turnCount++;
                Wygrana();
                compTurn();

            }

            private void compTurn()
            {

                    Ruchy();
          
                turnCount++;
                Przegrana();
            }
            private void Wygrana()
            {
                if (cPieciomasztowiecCount == 0)
                {
                    cPieciomasztowiecCount = -1;
                    MessageBox.Show("Zatopiłeś mój Pieciomasztowiec!");
                }
                if (cTrojmasztowiec2Count == 0)
                {
                    cTrojmasztowiec2Count = -1;
                    MessageBox.Show("Zatopiłeś mój Trojmasztowiec!");
                }
                if (cDwumasztowiecCount == 0)
                {
                    cDwumasztowiecCount = -1;
                    MessageBox.Show("Zatopiłeś mój Dwumasztowiec!");
                }
                if (cCzteromasztowiecCount == 0)
                {
                    cCzteromasztowiecCount = -1;
                    MessageBox.Show("Zatopiłeś mój Czteromasztowiec!");
                }
                if (cTrojmasztowiec1Count == 0)
                {
                    cTrojmasztowiec1Count = -1;
                    MessageBox.Show("Zatopiłeś mój Trojmasztowiec!");
                }

                if (cPieciomasztowiecCount == -1 && cCzteromasztowiecCount == -1 && cTrojmasztowiec1Count == -1 &&
                    cTrojmasztowiec2Count == -1 && cDwumasztowiecCount == -1)
                {
                    MessageBox.Show("Wygrałeś!");
                    disableGrids();
                saveHighScores(true);
                }
            }



            private void Przegrana()
            {
                if (pPieciomasztowiecCount == 0)
                {
                    pPieciomasztowiecCount = -1;
                    MessageBox.Show("Twój Pieciomasztowiec został zatopiony!");
                }
                if (pTrojmasztowiec2Count == 0)
                {
                    pTrojmasztowiec2Count = -1;
                    MessageBox.Show("Twój Trojmasztowiec został zatopiony!");
                }
                if (pDwumasztowiecCount == 0)
                {
                    pDwumasztowiecCount = -1;
                    MessageBox.Show("Twój Dwumasztowiec został zatopiony!");
                }
                if (pCzteromasztowiecCount == 0)
                {
                    pCzteromasztowiecCount = -1;
                    MessageBox.Show("Twoj Czteromasztowiec został zatopiony!");
                }
                if (pTrojmasztowiec1Count == 0)
                {
                    pTrojmasztowiec1Count = -1;
                    MessageBox.Show("Twoj Trojmasztowiec został zatopiony!");
                }

                if (pPieciomasztowiecCount == -1 && pCzteromasztowiecCount == -1 && pTrojmasztowiec1Count == -1 &&
                    pTrojmasztowiec2Count == -1 && pDwumasztowiecCount == -1)
                {
                    MessageBox.Show("Przegrałeś!");
                    disableGrids();
                saveHighScores(false);
                }
            }
            private void disableGrids()
            {
                foreach (var element in compGrid)
                {
                    if (element.Tag.Equals("woda"))
                    {
                        element.Background = new SolidColorBrush(Colors.LightGray);
                    }
                    else if (element.Tag.Equals("pieciomasztowiec") || element.Tag.Equals("trojmasztowiec2") ||
                      element.Tag.Equals("dwumasztowiec") || element.Tag.Equals("czteromasztowiec") || element.Tag.Equals("trojmasztowiec1"))
                    {
                        element.Background = new SolidColorBrush(Colors.LightGreen);
                    }
                    element.IsEnabled = false;
                }
                foreach (var element in playerGrid)
                {
                    if (element.Tag.Equals("woda"))
                    {
                        element.Background = new SolidColorBrush(Colors.LightGray);
                    }
                    element.IsEnabled = false;
                }

            }
            private string validateXCoordinate(string X)
            {
                if (X.Length != 1)
                {
                    return "";
                }

                if (Char.IsLetter(X[0]))
                {
                    return X;
                }
                return "";
            }

        private void tabela_Click(object sender, RoutedEventArgs e)
        {
            Tabela tabela = new Tabela();
            tabela.Show();
        }




        /*  private string validateYCoordinate(string Y)
              {
                  if (Y.Length > 2 || Y == "")
                  {
                      return "";
                  }

                  if (int.Parse(Y) > 0 || int.Parse(Y) <= 10)
                  {
                      return Y;
                  }
                  return "";
              }*/




        private void Ruchy()
            {

                if (hitList.Count == 0)
                {
                    
                     Wyszukiwanie();
            }

                else
                    Zatapianie();
            }

        private void Wyszukiwanie()
        {
            int position;
            do
            {
                position = random.Next(100);
                Console.WriteLine(playerGrid[position].Tag);
              
            } while ((playerGrid[position].Tag.Equals("pudło")) || (playerGrid[position].Tag.Equals("trafiony")));

                Strzal(position);
            

        }


        private void Strzal(int position)
            {

                if (!(playerGrid[position].Tag.Equals("woda")))
                {

                    if (hitList != null && hitList.Contains(position))
                        hitList.Remove(position);

    
                    switch (playerGrid[position].Tag.ToString())
                    {
                        case "dwumasztowiec":
                            pDwumasztowiecCount--;
                            break;
                        case "trojmasztowiec2":
                            pTrojmasztowiec2Count--;
                            break;
                        case "trojmasztowiec1":
                            pTrojmasztowiec1Count--;
                            break;
                        case "czteromasztowiec":
                            pCzteromasztowiecCount--;
                            break;
                        case "pieciomasztowiec":
                            pPieciomasztowiecCount--;
                            break;
                    }
      
                    playerGrid[position].Tag = "trafiony";
                    playerGrid[position].Background = new SolidColorBrush(Colors.Red);

                   
                    if (pDwumasztowiecCount == 0 || pTrojmasztowiec2Count == 0 || pTrojmasztowiec1Count == 0 || pCzteromasztowiecCount == 0 || pPieciomasztowiecCount == 0)
                    {
                        hitList.Clear();
                    }
   
                    else
                    {

                        if (position % 10 == 0)
                            hitList.Add(position + 1);
     
                        else if (position % 10 == 9)
                            hitList.Add(position - 1);
                      
                        else
                        {
                            hitList.Add(position + 1);
                            hitList.Add(position - 1);
                        }
             
                        if (position < 10)
                            hitList.Add(position + 10);
                    
                        else if (position > 99)
                            hitList.Add(position - 10);
                
                        else
                        {
                            hitList.Add(position + 10);
                            hitList.Add(position - 10);
                        }

                 
                        try
                        {
                            hitList.Remove(position - 11);
                        }
                        catch (Exception e) { }
                        try
                        {
                            hitList.Remove(position - 9);
                        }
                        catch (Exception e) { }
                        try
                        {
                            hitList.Remove(position + 9);
                        }
                        catch (Exception e) { }
                        try
                        {
                            hitList.Remove(position + 11);
                        }
                        catch (Exception e) { }
                    }
                }
                else
                {
                    playerGrid[position].Tag = "pudło";
                    playerGrid[position].Background = new SolidColorBrush(Colors.LightGray);
                }
            }


            private void Zatapianie()
            {
                int position;
         
                do
                {
                    
                    position = random.Next(hitList.Count);
                } while (playerGrid[hitList[position]].Tag.Equals("pudło") || playerGrid[hitList[position]].Tag.Equals("trafiony"));

             
               
                Strzal(hitList[position]);

            }




        private void restartuj_Click(object sender, RoutedEventArgs e)
        {
            replay(this, e);
        }

        private List<string> saveHighScores(bool playerWins)
        {
            String filename = @"../../scores.txt";
            string[] user = { playerName, "0", "0" };
            string[] playerNames;
            int index;
            int wins = 0;
            int losses = 0;

            
            if (!File.Exists(filename))
            {
                FileStream stream = File.Create(filename);
                stream.Close();
            }

            
            List<string> players = new List<string>(File.ReadAllLines(filename));

            playerNames = new string[players.Count];

            for (index = 0; index < players.Count; index++)
            {
                playerNames[index] = players[index].Split(' ')[0];
            }
            
            index = binarySearch(playerNames, playerName);

            if (index > -1)
            {
                user = players[index].Split();
                players.RemoveAt(index);
            }
            else
            {
                index = -(index + 1);
            }
            if (playerWins == true)
            {
                wins = int.Parse(user[1]) + 1;
            }
            else
            {
                losses = int.Parse(user[2]) + 1;
            }
            players.Insert(index, playerName + " " + wins + " " + losses);

            File.WriteAllLines(filename, players);
            return players;
        }

        private int binarySearch(string[] players, string value)
        {

            int low = 0;
            int high = players.Length - 1;

            while (high >= low)
            {
                int middle = (low + high) / 2;

                if (players[middle].CompareTo(value) == 0)
                {
                    return middle;
                }
                if (players[middle].CompareTo(value) < 0)
                {
                    low = middle + 1;
                }
                if (players[middle].CompareTo(value) > 0)
                {
                    high = middle - 1;
                }
            }
            return -(low + 1);
        }


    }
    }
