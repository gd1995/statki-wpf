﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Statki
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Grid grid = new Grid();

        private AddShips add_ship;
        private Bitwa bitwa;
        private Start start;

        public MainWindow()
        {
            InitializeComponent();
            InitializeGame();
        }

        private void InitializeGame()
        {
            //Initialize window
            Content = grid;

            this.MinHeight = 300;
            this.MinWidth = 330;
            this.Height = 300;
            this.Width = 330;

       
            start = new Start();
            grid.Children.Add(start);


            start.play += new EventHandler(shipSetup);
        }


        private void shipSetup(object sender, EventArgs e)
        {

            Content = grid;

            

            this.Width = 460;
            this.Height = 690;

            
            add_ship = new AddShips();

            
            grid.Children.Add(add_ship);

            add_ship.start += new EventHandler(playGame);
        }


        private void playGame(object sender, EventArgs e)
        {
            
            grid.Children.Clear();

            
            this.MinWidth = 953.286;
            this.MinHeight = 480;
            this.Width = 953.286;
            this.Height = 480;

            
            bitwa = new Bitwa(add_ship.grid, start.name);

            
            grid.Children.Add(bitwa);
            bitwa.replay += new EventHandler(replayGame);

        }

        private void replayGame(object sender, EventArgs e)
        {
            
            grid.Children.Clear();
            InitializeGame();
        }
    }
}
