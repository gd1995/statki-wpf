﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Statki
{
    /// <summary>
    /// Interaction logic for AddShips.xaml
    /// </summary>
    public partial class AddShips : UserControl
    {
        public event EventHandler start;

        enum Orientation { VERTICAL, HORIZONTAL };
        Orientation orientacja = Orientation.HORIZONTAL;
        SolidColorBrush niewybrany = new SolidColorBrush(Colors.Black);
        SolidColorBrush wybrany = new SolidColorBrush(Colors.Green);
        String statek = "";
        int wielkosc;
        int ile_dodano;
        Path ostatni_statek;
        Path[] statki;
        public Grid[] grid;

        SolidColorBrush kolor_statku = (SolidColorBrush)(new BrushConverter().ConvertFrom("#88cc00"));

        public AddShips()
        {
            InitializeComponent();

            grid = new Grid[] { gridA1, gridA2, gridA3, gridA4, gridA5, gridA6, gridA7, gridA8, gridA9, gridA10,
                                gridB1, gridB2, gridB3, gridB4, gridB5, gridB6, gridB7, gridB8, gridB9, gridB10,
                                gridC1, gridC2, gridC3, gridC4, gridC5, gridC6, gridC7, gridC8, gridC9, gridC10,
                                gridD1, gridD2, gridD3, gridD4, gridD5, gridD6, gridD7, gridD8, gridD9, gridD10,
                                gridE1, gridE2, gridE3, gridE4, gridE5, gridE6, gridE7, gridE8, gridE9, gridE10,
                                gridF1, gridF2, gridF3, gridF4, gridF5, gridF6, gridF7, gridF8, gridF9, gridF10,
                                gridG1, gridG2, gridG3, gridG4, gridG5, gridG6, gridG7, gridG8, gridG9, gridG10,
                                gridH1, gridH2, gridH3, gridH4, gridH5, gridH6, gridH7, gridH8, gridH9, gridH10,
                                gridI1, gridI2, gridI3, gridI4, gridI5, gridI6, gridI7, gridI8, gridI9, gridI10,
                                gridJ1, gridJ2, gridJ3, gridJ4, gridJ5, gridJ6, gridJ7, gridJ8, gridJ9, gridJ10,
                                gridK1, gridK2, gridK3, gridK4, gridK5, gridK6, gridK7, gridK8, gridK9, gridK10};
            statki = new Path[] { dwumasztowiec, trojmasztowiec1, trojmasztowiec2, czteromasztowiec, pieciomasztowiec };
            reset();
        }
        private void reset()
        {

            foreach (var element in grid)
            {
                element.Tag = "woda";
                element.Background = new SolidColorBrush(Colors.White);
            }

            foreach (var element in statki)
            {
                element.IsEnabled = true;
                element.Opacity = 110;
                if (element.Stroke != niewybrany)
                {
                    element.Stroke = niewybrany;
                }
            }
            ile_dodano = 0;
            ostatni_statek = null;
        }

        private void ship_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Path shipPath = (Path)sender;
            if (!shipPath.IsEnabled)
            {
                return;
            }
            if (ostatni_statek != null)
            {
                ostatni_statek.Stroke = niewybrany;
            }

            ostatni_statek = shipPath;
            statek = shipPath.Name;
            shipPath.Stroke = wybrany;

            switch (statek)
            {
                case "pieciomasztowiec":
                    wielkosc = 5;
                    break;
                case "czteromasztowiec":
                    wielkosc = 4;
                    break;
                case "trojmasztowiec1":
                case "trojmasztowiec2":
                    wielkosc = 3;
                    break;
                case "dwumasztowiec":
                    wielkosc = 2;
                    break;
            }
        }

        private void gridMouseDown(object sender, MouseButtonEventArgs e)
        {
            Grid square = (Grid)sender;
            int index = -1;
            int temp = 0;
            int licznik = 1;

            if (ostatni_statek == null)
            {
                MessageBox.Show("Musisz wybrać statek", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!square.Tag.Equals("woda"))
            {
                return;
            }
            index = Array.IndexOf(grid, square);


            if (orientacja.Equals(Orientation.HORIZONTAL))
            {
                try
                {
                    licznik = 1;
                    for (int i = 0; i < wielkosc; i++)
                    {
                        if (index + i <= 109)
                        {
                            if (index > 9)
                            {
                                if (!grid[index - 10].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }
                                if (!grid[index - 9].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }
                                if (!grid[index + i - 9].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }

                                if (index % 10 != 0)
                                {
                                    if (!grid[index - 1].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index - 11].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                }

                            }


                            if (!grid[index + i + 1].Tag.Equals("woda"))
                            {
                                throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                            }

                            if (index < 99)
                            {
                                if (!grid[index + i + 10].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }
                                if (!grid[index + 9].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }

                                if (!grid[index + i + 11].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }
                            }


                            if (index < 9 && index != 0)
                            {
                                if (!grid[index - 1].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }
                            }

                        }


                        else
                        {
                            if (!grid[index - licznik].Tag.Equals("woda"))
                            {
                                throw new IndexOutOfRangeException("Nieprawidłowe umiejscowienie statku");
                            }
                            licznik++;
                        }

                    }

                }
                catch (IndexOutOfRangeException iore)
                {
                    MessageBox.Show(iore.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

            }
            else 
            {
                try
                {
                    licznik = 10;
                    for (int i = 0; i < wielkosc * 10; i += 10)
                    {
                        if (index + i <= 109)
                        {
                            if (index > 9)
                            {
                                if (index % 10 != 0)
                                {
                                    if (!grid[index - 1].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index - 10].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }

                                    if (!grid[index - 11].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if ((index + 1) % 10 != 0)
                                    {
                                        if (!grid[index + 1].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }
                                        if (!grid[index - 9].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }
                                        if ((index + i) < 99)
                                        {
                                            if (!grid[index + i + 11].Tag.Equals("woda"))
                                            {
                                                throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                            }
                                            if (!grid[index + 11].Tag.Equals("woda"))
                                            {
                                                throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                            }
                                        }
                                    }
                                    if ((index + i) < 99)
                                    {
                                        if (!grid[index + i + 10].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }
                                        if (!grid[index + i + 9].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }
                                        if (!grid[index + 9].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }

                                    }
                                }


                                else
                                {
                                    if (!grid[index + 1].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index - 9].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index - 10].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index + 11].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }

                                    if ((index + i) < 99)
                                    {
                                        if (!grid[index + i + 10].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }
                                        if (!grid[index + i + 11].Tag.Equals("woda"))
                                        {
                                            throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                        }
                                    }
                                }

                            }

                            else
                            {
                                if (!grid[index + i + 10].Tag.Equals("woda"))
                                {
                                    throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                }
                                if (index != 0)
                                {
                                    if (!grid[index - 1].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index + 9].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                }
                                if (index != 9)
                                {
                                    if (!grid[index + 1].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                    if (!grid[index + 11].Tag.Equals("woda"))
                                    {
                                        throw new IndexOutOfRangeException("Niewystarczająca ilość miejsca!");
                                    }
                                }
                            }



                        }

                        else
                        {
                            if (!grid[index - licznik].Tag.Equals("woda"))
                            {
                                throw new IndexOutOfRangeException("Nieprawidłowe umiejscowienie statku");
                            }
                            licznik += 10;
                        }
                    }
                    if ((index / 10) + (wielkosc * 10) > 110)
                    {
                        throw new IndexOutOfRangeException("Nieprawidłowe umiejscowienie statku");
                    }
                }
                catch (IndexOutOfRangeException iore)
                {
                    MessageBox.Show(iore.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

            }

            if (orientacja.Equals(Orientation.HORIZONTAL))
            {
                //If two rows
                if ((index + wielkosc - 1) % 10 < wielkosc - 1)
                {
                    licznik = 0;

                    for (int i = licznik; i < wielkosc; i++)
                    {
                        temp = 1;
                    }

                    if (temp == 0)
                    {
                        while ((index + licznik) % 10 > 1)
                        {
                            grid[index + licznik].Background = kolor_statku;
                            grid[index + licznik].Tag = statek;
                            licznik++;
                        }
                    }

                }

                else
                {
                    for (int i = 0; i < wielkosc; i++)
                    {
                        grid[index + i].Background = kolor_statku;
                        grid[index + i].Tag = statek;
                    }
                }
            }
            else
            {

                if (index + (wielkosc * 10) > 110)
                {
                    licznik = 0;
                    for (int i = licznik; i < wielkosc; i++)
                    {
                        temp = 1;
                    }
                    if (temp == 0)
                    {
                        while ((index / 10 + licznik) % 110 < 10)
                        {
                            grid[index + licznik * 10].Background = kolor_statku;
                            grid[index + licznik * 10].Tag = statek;
                            licznik++;
                        }
                    }

                }
                //If one column
                else
                {
                    licznik = 0;
                    for (int i = 0; i < wielkosc * 10; i += 10)
                    {
                        grid[index + i].Background = kolor_statku;
                        grid[index + i].Tag = statek;
                    }
                }
            }
            if (temp == 0)
            {
                ostatni_statek.IsEnabled = false;
                ostatni_statek.Opacity = 0.5;
                ostatni_statek.Stroke = niewybrany;
                ostatni_statek = null;
                ile_dodano++;
            }
            temp = 0;
        }




        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (ile_dodano != 5)
            {
                return;
            }
            start(this, e);
        }

        private void Losowo_Click(object sender, RoutedEventArgs e)
        {
            reset();
            Random random = new Random();
            int[] shipSizes = new int[] { 2, 3, 3, 4, 5 };
            string[] shipNames = new string[] { "dwumasztowiec", "trojmasztowiec1", "trojmasztowiec2", "czteromasztowiec", "pieciomasztowiec" };
            int size, index;
            string ship;
            Orientation orientation;
            bool unavailableIndex = true;


            for (int i = 0; i < shipSizes.Length; i++)
            {
                //Set size and ship type
                size = shipSizes[i];
                ship = shipNames[i];
                unavailableIndex = true;

                if (random.Next(0, 2) == 0)
                    orientation = Orientation.HORIZONTAL;
                else
                    orientation = Orientation.VERTICAL;


                if (orientation.Equals(Orientation.HORIZONTAL))
                {
                    index = random.Next(0, 110);
                    while (unavailableIndex == true)
                    {
                        unavailableIndex = false;

                        while ((index + size - 1) % 10 < size - 1)
                        {
                            index = random.Next(0, 110);
                        }

                        for (int j = 0; j < size; j++)
                        {
                            if (index + j > 99)
                            {
                                index = random.Next(0, 110);
                                unavailableIndex = true;
                                break;
                            }
                                if (!grid[index + j].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }

                                if (index > 9)
                                {
                                    if (!grid[index - 10].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index - 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + j - 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }

                                    if (index % 10 != 0)
                                    {
                                        if (!grid[index - 1].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!grid[index - 11].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                    }

                                }


                                if (!grid[index + j + 1].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }

                                if (index < 93)
                                {
                                    if (!grid[index + j + 10].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }

                                    if (!grid[index + j + 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }


                                if (index < 9 && index != 0)
                                {
                                    if (!grid[index - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }

                            

                        }
                    }
                    for (int j = 0; j < size; j++)
                    {
                        grid[index + j].Tag = ship;
                        grid[index + j].Background = kolor_statku;
                    }
                }
                else
                {
                    index = random.Next(0, 110);
                    while (unavailableIndex == true)
                    {
                        unavailableIndex = false;

                        while (index / 10 + size * 10 > 110)
                        {
                            index = random.Next(0, 110);
                        }

                        for (int j = 0; j < size * 10; j += 10)
                        {
                            if (index + j > 99 )
                            {
                                index = random.Next(0, 110);
                                unavailableIndex = true;
                                break;
                            }
                            if (!grid[index + j].Tag.Equals("woda"))
                            {
                                index = random.Next(0, 110);
                                unavailableIndex = true;
                                break;
                            }


                            if (index > 9)
                            {
                                if (index % 10 != 0)
                                {
                                    if (!grid[index - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + j - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index - 10].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }

                                    if (!grid[index - 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if ((index + 1) % 10 != 0)
                                    {
                                        if (!grid[index + 1].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!grid[index + j + 1].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!grid[index - 9].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if ((index + j) < 99)
                                        {
                                            if (!grid[index + j + 11].Tag.Equals("woda"))
                                            {
                                                index = random.Next(0, 110);
                                                unavailableIndex = true;
                                                break;
                                            }
                                            if (!grid[index + 11].Tag.Equals("woda"))
                                            {
                                                index = random.Next(0, 110);
                                                unavailableIndex = true;
                                                break;
                                            }
                                        }
                                    }
                                    if ((index + j) < 99)
                                    {
                                        if (!grid[index + j + 10].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!grid[index + j + 9].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!grid[index + 9].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }

                                    }
                                }


                                else
                                {
                                    if (!grid[index + 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + j + 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index - 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index - 10].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }

                                    if ((index + j) < 99)
                                    {
                                        if (!grid[index + j + 10].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                        if (!grid[index + j + 11].Tag.Equals("woda"))
                                        {
                                            index = random.Next(0, 110);
                                            unavailableIndex = true;
                                            break;
                                        }
                                    }
                                }

                            }

                            else
                            {
                                if (!grid[index + j + 10].Tag.Equals("woda"))
                                {
                                    index = random.Next(0, 110);
                                    unavailableIndex = true;
                                    break;
                                }
                                if (index != 0)
                                {
                                    if (!grid[index - 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + 9].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }
                                if (index != 9)
                                {
                                    if (!grid[index + 1].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                    if (!grid[index + 11].Tag.Equals("woda"))
                                    {
                                        index = random.Next(0, 110);
                                        unavailableIndex = true;
                                        break;
                                    }
                                }
                            }








                        }
                    }
                    for (int j = 0; j < size * 10; j += 10)
                    {
                        grid[index + j].Tag = ship;
                        grid[index + j].Background = kolor_statku;
                    }
                }

            }
            ile_dodano = 5;
            foreach (var element in statki)
            {
                element.IsEnabled = false;
                element.Opacity = .5;
                if (element.Stroke != niewybrany)
                {
                    element.Stroke = niewybrany;
                }

            }
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            reset();
        }

        private void Poziom_Checked(object sender, RoutedEventArgs e)
        {
            orientacja = Orientation.HORIZONTAL;
        }

        private void Pion_Checked(object sender, RoutedEventArgs e)
        {
            orientacja = Orientation.VERTICAL;
        }
    }
}
